
package AI_2;

/**
 * Permite definir las coordenadas de un punto, representativo  de las funciones de pertenencia.
 * Eje x abscisas: valor numérico.
 * Eje y ordenadas: valor de pertenencia correspondiente entre 0 y 1.
 */
public class Punto2D {
    
    public double x,y;

    public Punto2D(double _x,double _y){
        x = _x;
        y = _y;
    }

    @Override
    public String toString() {
        return "(" + x + ";" + y + ")";
    }

    
}
