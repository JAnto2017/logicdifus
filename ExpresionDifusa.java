package AI_2;

public class ExpresionDifusa {
    protected VariableLinguistica varL;
    protected String nombreValorLinguistico;
    //constructor
    public ExpresionDifusa(VariableLinguistica _vl, String _valor) {
        varL = _vl;
        nombreValorLinguistico = _valor;
    }
}

/**
 * Para expresar la forma "variable IS valor"
 * Para ello asociamos variable lingüística varL
 * y una cadena correspondiente al valor lingüístico deseado - nombreValorLinguistico -
 */