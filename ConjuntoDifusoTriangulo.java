package AI_2;

public class ConjuntoDifusoTriangulo extends ConjuntoDifuso{
    //constuctor
    public ConjuntoDifusoTriangulo(double min,double max,double inicioBase,double cima,double finBase) {
        super(min, max);
        Agregar(new Punto2D(min, 0));
        Agregar(new Punto2D(inicioBase, 0));
        Agregar(new Punto2D(cima, 1));
        Agregar(new Punto2D(finBase, 0));
        Agregar(new Punto2D(max, 0));
    }
}

/**
 * Necesita 3 puntos de interes: 
 * el comienzo del triángulo - inicioBase -
 * el punto culminante - cima -
 * el final del triángulo - finBase -
 */
