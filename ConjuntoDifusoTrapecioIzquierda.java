package AI_2;

public class ConjuntoDifusoTrapecioIzquierda extends ConjuntoDifuso{
    //constructor
    public ConjuntoDifusoTrapecioIzquierda(double min, double max, double finPlanoElevado, double inicioPlanoBajo) {
        super(min, max);
        Agregar(new Punto2D(min, 1));
        Agregar(new Punto2D(finPlanoElevado, 1));
        Agregar(new Punto2D(inicioPlanoBajo, 0));
        Agregar(new Punto2D(max, 0));
    }
}

/**
 * Requiere intervalo de definición, coordenada del último punto con altura 1 y del primero con altura 0.
 */
