package AI_2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.util.StringJoiner;

public class ConjuntoDifuso {
    protected ArrayList<Punto2D> puntos;
    protected double min, max;

    //constructor
    public ConjuntoDifuso(double _min,double _max){
        min = _min;
        max = _max;
    }

    //agregar un punto -------------------------------------------------------------------------------------
    public void Agregar(Punto2D pt) {
        puntos.add(pt);
        //Collections.sort();
    }

    public void Agregar(double x,double y) {
        Punto2D pt = new Punto2D(x, y);
        Agregar(pt);
    }

    //método toString
    /**
     * Muestra el intervalo de valores y a continuación, los distintos puntos registrados en la lista
     */
    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(" ");
        sj.add("[" + min + "-" + max + "]:");

        for (Punto2D pt : puntos) {
            sj.add(pt.toString());
        }
        return sj.toString();
    }

    /**
     * Permite saber si dos conjuntos difusos son iguales.
     * Serán idénticos si producen la misma cadena.
     * Así evitamos compara todos los puntos uno a uno.
     */
    @Override
    public boolean equals(Object obj) {
        return toString().equals(((Punto2D)obj).toString());
    }

    /**
     * Multiplica todas las ordenadas de los puntos por el valor pasado como parámetro y agregarlos en un nuevo conjunto difuso que será devuelto.
     * @param valor
     * @return
     */
    public ConjuntoDifuso MultiplicarPor(double valor) {
        ConjuntoDifuso conjunto = new ConjuntoDifuso(min, max);

        for (Punto2D pt : puntos) {
            conjunto.Agregar(pt.x, pt.y * valor);
        }

        return conjunto;
    }

    //operador NOT
    /**
     * Creamos conjunto difuso sobre el mismo intervalo.
     * Agregamos para cada punto del conjunto de partida, un punto de altura 1-y
     * @return
     */
    public ConjuntoDifuso No() {
        ConjuntoDifuso conjunto = new ConjuntoDifuso(min, max);

        for (Punto2D pt : puntos) {
            conjunto.Agregar(pt.x, 1 - pt.y);
        }

        return conjunto;
    }

    //cálculo del grado de pertenencia de un punto
    /**
     * Para determinar el grado de pertenencia de un valor numérico, pueden darse tres casos:
     * 1.- Valor fuera del intervalo, grado de pertenencia es nulo.
     * 2.- Definido punto para este valor en el conjunto difuso. Devuelve el grado registrado.
     * 3.- No se ha definido ningún punto para este valor. Preciso interpolar grado de pertenencia.
     * @param valor
     * @return
     */
    public double ValorDePertenencia(double valor) {
        //caso 1: al exterior del intervalo del conjunto difuso
        if (valor < min || valor > max || puntos.size() < 2) {
            return 0;
        }

        Punto2D ptAntes = puntos.get(0);
        Punto2D ptDespues = puntos.get(1);

        int index = 0;

        while (valor >= ptDespues.x) {
            index++;
            ptAntes = ptDespues;
            ptDespues = puntos.get(index);
        }

        if (ptAntes.x == valor) {
            //tenemos punto en este valor
            return ptAntes.y;
        } else {
            //se aplica la interpolación
            return ((ptAntes.y - ptDespues.y) * (ptDespues.x - valor) / (ptDespues.x - ptAntes.x) + ptDespues.y);
        }
    }

    //método min ó max
    /**
     * Permite fusionar dos conjuntos, cuyo nombre se pasa como parámetro.
     * Recibe dos valores.
     * Devuelve el valor que hay que guardar.
     * @param valor1
     * @param valor2
     * @param metodo
     * @return
     */
    private static double Optimo(double valor1, double valor2, String metodo) {
        if (metodo.equals("Min")) {
            return Math.min(valor1, valor2);
        } else {
            return Math.max(valor1, valor2);
        }
    }

    //método genérico -------------------------------------------------------------------------------------------
    private static ConjuntoDifuso Fusionar(ConjuntoDifuso c1, ConjuntoDifuso c2, String metodo) {
        //creación del método
        ConjuntoDifuso resultado = new ConjuntoDifuso(Math.min(c1.min, c2.min), Math.max(c1.max, c2.max));

        //se van a recorrer las listas mediante los iteradores
        Iterator<Punto2D> iterador1 = c1.puntos.iterator();
        Punto2D ptConjunto1 = iterador1.next();

        Punto2D antiguoPtConjunto1 = ptConjunto1;
        Iterator<Punto2D> iterador2 = c2.puntos.iterator();
        Punto2D ptConjunto2 = iterador2.next();

        //se calcula la posición relativa de las dos curvas
        int antiguaPosicionRelativa;
        int nuevaPosicionRelativa = (int) Math.signum(ptConjunto1.y - ptConjunto2.y);

        boolean lista1Interminada = false;
        boolean lista2Interminada = false;

        //bucle sobre todos los puntos de ambas colecciones
        while (!lista1Interminada && !lista2Interminada) {
            //se recuperan las abscisas de los puntos en curso
            double x1 = ptConjunto1.x;
            double x2 = ptConjunto2.x;

            //cálculo de las posiciones relativa
            antiguaPosicionRelativa = nuevaPosicionRelativa;
            nuevaPosicionRelativa = (int) Math.signum(ptConjunto1.y - ptConjunto2.y);

            //Curvas invertidas?
            //Si no, ¿se tiene un solo punto
            //a tener en cuenta?
            if (antiguaPosicionRelativa != 0 && nuevaPosicionRelativa !=0) {
                //se debe calcular el punto de intersección
                double x = (x1 == x2 ? antiguoPtConjunto1.x : Math.min(x1, x2));
                double xPrima = Math.max(x1, x2);

                //calculo de las pendientes
                double p1 = c1.ValorDePertenencia(xPrima) - c1.ValorDePertenencia(x) / (xPrima - x);
                double p2 = c2.ValorDePertenencia(xPrima) - c2.ValorDePertenencia(x) / (xPrima - x);

                //calculo de la delta
                double delta = 0;
                if ((p2-p1) != 0) {
                    delta = (c2.ValorDePertenencia(x) - c1.ValorDePertenencia(x) / (p1 - p2));
                }

                //se agrega un punto de intersección al resultado
                resultado.Agregar(x + delta, c1.ValorDePertenencia(x + delta));

                //se pasa al punto siguiente
                if (x1 < x2) {
                    antiguoPtConjunto1 = ptConjunto1;
                    if (iterador1.hasNext()) {
                        ptConjunto1 = iterador1.next();
                    } else {
                        lista1Interminada = true;
                        ptConjunto1 = null;
                    }
                } else if(x1 > x2) {
                    if (iterador2.hasNext()) {
                        ptConjunto2 = iterador2.next();
                    } else {
                        ptConjunto2 = null;
                        lista2Interminada = true;
                    }
                }
            } else if (x1 == x2) {
                //dos puntos en la misma abscisa
                //basta con guardar el bueno
                resultado.Agregar(x1, Optimo(ptConjunto1.y, ptConjunto2.y, metodo));
                //se pasa al punto siguiente
                if (iterador1.hasNext()) {
                    antiguoPtConjunto1 = ptConjunto1;
                    ptConjunto1 = iterador1.next();
                } else {
                    ptConjunto1 = null;
                    lista1Interminada = true;
                }
                if (iterador2.hasNext()) {
                    ptConjunto2 = iterador2.next();
                } else {
                    ptConjunto2 = null;
                    lista2Interminada = true;
                }
            } else if (x1 < x2) {
                //la curva C1 tiene un punto antes
                //se calcula el grado para el segundo
                //y se guarda el bueno
                resultado.Agregar(x1, Optimo(ptConjunto1.y, c2.ValorDePertenencia(x1), metodo));
                //se desplaza
                if (iterador1.hasNext()) {
                    antiguoPtConjunto1 = ptConjunto1;
                    ptConjunto1 = iterador1.next();
                } else {
                    ptConjunto1 = null;
                    lista1Interminada = true;
                }
            } else {
                //ultimo caso, la curva C2
                //tiene un punto antes
                //se calcula el grado para el primero
                //y se guarda el bueno
                resultado.Agregar(x2,Optimo(c1.ValorDePertenencia(x2), ptConjunto2.y, metodo));
                //se desplaza
                if (iterador2.hasNext()) {
                    ptConjunto2 = iterador2.next();
                } else {
                    ptConjunto2 = null;
                    lista2Interminada = true;
                }
            }
        }
        //aquí, al menos una de las listas se ha terminado
        //se agregan los puntos restantes
        if (!lista1Interminada) {
            while (iterador1.hasNext()) {
                ptConjunto1 = iterador1.next();
                resultado.Agregar(ptConjunto1.x, Optimo(ptConjunto1.y, 0, metodo));
            }
        } else if (!lista2Interminada) {
            while (iterador2.hasNext()) {
                ptConjunto2 = iterador2.next();
                resultado.Agregar(ptConjunto2.x, Optimo(ptConjunto2.y, 0, metodo));
            }
        }

        return resultado;
    }

    // Operador Y
    public ConjuntoDifuso Y(ConjuntoDifuso c2) {
        return Fusionar(this, c2, "Min");
    }

    // Operador O
    public ConjuntoDifuso O(ConjuntoDifuso c2) {
        return Fusionar(this, c2, "Max");
    }

    /**
     * Determina el centroide = centro de gravedad del conjunto para realizar la defuzzificacion
     * En realidad es la coordenada en x de este punto lo que se busca.
     * Es preciso calcular para cada forma las coordenadas de su centroide, así como su área.
     * Guardamos en una variable el área total, y en otra la suma de las áreas ponderadas por las coordenadas.
     * Bastará a continuación, con realizar la división para obtener la abscisa buscada.
     * 
     * @return
     */
    public double Baricentro() {
        //si hay menos de dos puntos, no hay Baricentro
        if (puntos.size() <= 2) {
            return 0;
        } else {
            //inicialización de las áreas
            double areaPonderada = 0;
            double areaTotal = 0;
            double areaLocal;
            //recorrer lista conservando 2 puntos
            Punto2D antiguoPt = null;

            for (Punto2D pt : puntos) {
                if (antiguoPt != null) {
                    //calculo baricentro local
                    if (antiguoPt.y == pt.y) {
                        //es un rectangulo, el baricentro esta en el centro
                        areaLocal = pt.y * (pt.x - antiguoPt.x);
                        areaTotal += areaLocal;
                        areaPonderada += areaLocal * ((pt.x - antiguoPt.x) / 2.0 + antiguoPt.x);
                    } else {
                        //es un trapecio, que podemos descomponer en un rectángulo con un triángulo
                        //rectángulo adicional.
                        //Separamos ambas formas. Primer tiempo: rectángulo
                        areaLocal = Math.min(pt.y, antiguoPt.y) * (pt.x - antiguoPt.x);
                        areaTotal += areaLocal;
                        areaPonderada += areaLocal * ((pt.x - antiguoPt.x) / 2.0 + antiguoPt.x);

                        //segundo tiempo: triángulo rectángulo
                        areaLocal = (pt.x - antiguoPt.x) * Math.abs(pt.y - antiguoPt.y) / 2.0;
                        areaTotal += areaLocal;

                        if (pt.y > antiguoPt.y) {
                            //baricentro a 1/3 del lado pt
                            areaPonderada += areaLocal * (2.0/3.0 * (pt.x - antiguoPt.x) + antiguoPt.x);
                        } else {
                            //baricentro a 1/3 del lado antiguoPt
                            areaPonderada += areaLocal * (1.0/3.0 * (pt.x - antiguoPt.x) + antiguoPt.x);
                        }
                    }
                }
                antiguoPt = pt;
            }//for
            //devolvemos las coordenadas del baricentro
            return areaPonderada / areaTotal;
        }//if - else
        
    }//método
}
