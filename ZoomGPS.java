package AI_2;

public class ZoomGPS {
    public static void main(String[] args) {
        System.out.println("LÓGICA DIFUSA");

        //creación del sistema
        ControladorDifuso controlador = new ControladorDifuso("Gestión del zoom de un GPS");

        /**
         * Definición de las variables lingüísticas
         * Son 3: Distancia, Velocidad (in) y Zoom (out)
         * Se crean 3 variables lingüísticas:"Pequeña", "Media", "Grande"
         */

        System.out.println("Agregar variables de entrada");

        //
        VariableLinguistica distancia = new VariableLinguistica("Distancia", 0, 500000);

        distancia.AgregarValorLinguistico(new ValorLinguistico("Pequeña", new ConjuntoDifusoTrapecioIzquierda(0, 500000, 30, 50)));
        distancia.AgregarValorLinguistico(new ValorLinguistico("Media", new ConjuntoDifusoTrapecio(0, 500000, 40, 50, 100, 150)));
        distancia.AgregarValorLinguistico(new ValorLinguistico("Grande", new ConjuntoDifusoTrapecioDerecha(0, 500000, 100, 150)));
        controlador.AgregarVariableEntrada(distancia);

        //variable linguistica de entrada
        //velocidad en km de 0 a 200
        VariableLinguistica velocidad = new VariableLinguistica("Velocidad", 0, 200);

        velocidad.AgregarValorLinguistico(new ValorLinguistico("Lenta", new ConjuntoDifusoTrapecioIzquierda(0, 200, 20, 30)));
        velocidad.AgregarValorLinguistico(new ValorLinguistico("Media", new ConjuntoDifusoTrapecio(0, 200, 20, 30, 70, 80)));
        velocidad.AgregarValorLinguistico(new ValorLinguistico("Rapida", new ConjuntoDifusoTrapecio(0, 200, 70, 80, 90, 110)));
        velocidad.AgregarValorLinguistico(new ValorLinguistico("MuyRapida", new ConjuntoDifusoTrapecioDerecha(0, 200, 90, 110)));
        controlador.AgregarVariableEntrada(velocidad);

        System.out.println("Agregar la variable de salida");
        //variable linguistica de salida: nivel de zoom (1 a 5)
        VariableLinguistica zoom = new VariableLinguistica("Zoom", 0, 5);

        zoom.AgregarValorLinguistico(new ValorLinguistico("Bajo", new ConjuntoDifusoTrapecioIzquierda(0, 5, 1, 2)));
        zoom.AgregarValorLinguistico(new ValorLinguistico("Normal", new ConjuntoDifusoTrapecio(0, 5, 1, 2, 3, 4)));
        zoom.AgregarValorLinguistico(new ValorLinguistico("Grande", new ConjuntoDifusoTrapecioDerecha(0, 5, 3, 4)));
        controlador.AgregarVariableSalida(zoom);

        System.out.println("Agregar las reglas");
        //se agregan 9 reglas para 12 casos
        controlador.AgregarRegla("IF Distancia IS Grande THEN Zoom IS Bajo");
        controlador.AgregarRegla("IF Distancia IS Pequeña AND Velocidad IS Lenta THEN Zoom IS Normal");
        controlador.AgregarRegla("IF Distancia IS Pequeña AND Velocidad IS Media THEN Zoom IS Normal");
        controlador.AgregarRegla("IF Distancia IS Pequeña AND Velocidad IS Rapida THEN Zoom IS Grande");
        controlador.AgregarRegla("IF Distancia IS Pequeña AND Velocidad IS MuyRapida THEN Zoom Grande");
        controlador.AgregarRegla("IF Distancia IS Media AND Velocidad IS Lenta THEN Zoom IS Bajo");
        controlador.AgregarRegla("IF Distancia IS Media AND Velocidad IS Media THEN Zoom IS Normal");
        controlador.AgregarRegla("IF Distancia IS Media AND Velocidad IS Rapida THEN Zoom IS Normal");
        controlador.AgregarRegla("IF Distancia IS Media AND Velocidad IS MuyRapida THEN Zoom IS Grande");

        System.out.println("RESOLUCIÓN DE CASOS PRÁCTICOS -------------------------- ");
        //CASO PRACTICO 1
        System.out.println("Caso 1:");
        controlador.AgregarValorNumerico(velocidad, 35);
        controlador.AgregarValorNumerico(distancia, 70);
        System.out.println("Resultado: " + controlador.Resolver() + "\n");

        //CASO PRACTICO 2
        controlador.BorrarValoresNumericos();
        System.out.println("Caso 2:");
        controlador.AgregarValorNumerico(velocidad, 25);
        controlador.AgregarValorNumerico(distancia, 70);
        System.out.println("Resultado: " + controlador.Resolver() + "\n");

        //CASO PRACTICO 3
        controlador.BorrarValoresNumericos();
        System.out.println("Caso 3:");
        controlador.AgregarValorNumerico(velocidad, 72.5);
        controlador.AgregarValorNumerico(velocidad, 40);
        System.out.println("Resultado: " + controlador.Resolver() + "\n");

        //CASO PRACTICO 4
        controlador.BorrarValoresNumericos();
        System.out.println("Caso 4:");
        controlador.AgregarValorNumerico(velocidad, 100);
        controlador.AgregarValorNumerico(velocidad, 110);
        System.out.println("Resultado: " + controlador.Resolver() + "\n");

        //CASO PRACTICO 5
        controlador.BorrarValoresNumericos();
        System.out.println("Caso 5:");
        controlador.AgregarValorNumerico(velocidad, 45);
        controlador.AgregarValorNumerico(distancia, 160);
        System.out.println("Resultado: " + controlador.Resolver() + "\n");
    }
}
