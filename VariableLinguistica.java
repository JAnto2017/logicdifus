package AI_2;

import java.util.ArrayList;

/**
 * Representa variable linguística
 * Un nombre, rango de valores, lista de valores lingüísticos.
 * 4 atributos: nombre, valorMin, valorMax, values. Una lista de ValorLinguistico.
 * 3 métodos:
 *  => AgregarValorLinguistico. Agrega valor bien pasando objeto o creándolo a partir de un nombre y un conjunto difuso.
 *  => BorrarValores
 *  => ValorLinguisticoPorNombre
 * 
 * @author
 * @version
 */
public class VariableLinguistica {
    protected String nombre;
    protected ArrayList<ValorLinguistico> valores;
    protected double valorMin, valorMax;
    //constructor
    public VariableLinguistica(String _nombre,double _min,double _max){
        nombre = _nombre;
        valorMin = _min;
        valorMax = _max;
        valores = new ArrayList<>();
    }
    //-------------------------------------------------------------------------------------------
    public void AgregarValorLinguistico(ValorLinguistico vl) {
        valores.add(vl);        
    }
    //-------------------------------------------------------------------------------------------
    public void AgregarValorLinguistico(String nombre, ConjuntoDifuso conjunto) {
        valores.add(new ValorLinguistico(nombre, conjunto));
    }
    //-------------------------------------------------------------------------------------------
    public void BorrarValores() {
        valores.clear();
    }
    //-------------------------------------------------------------------------------------------
    ValorLinguistico valorLinguisticoPorNombre(String nombre){
        for (ValorLinguistico vl : valores) {
            if (vl.nombre.equalsIgnoreCase(nombre)) {
                return vl;
            }
        }
        return null;
    }
}
