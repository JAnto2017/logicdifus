package AI_2;

public class ConjuntoDifusoTrapecioDerecha extends ConjuntoDifuso{
    //constructor
    public ConjuntoDifusoTrapecioDerecha(double min,double max,double finPlanoBajo, double inicioPlanoElevado){
        super(min, max);
        Agregar(new Punto2D(min, 0));
        Agregar(new Punto2D(finPlanoBajo, 0));
        Agregar(new Punto2D(inicioPlanoElevado, 1));
        Agregar(new Punto2D(max, 1));
    }
}

/**
 * Principio igual.
 * Falta saber, hasta qué coordenada el grado es nulo - finPlanoBajo -
 * A partir de qué coordenada es 1 - inicioPlanoElevado -
 * 
 */