package AI_2;

import java.util.ArrayList;

import AI_1.ai_java_ii.Regla;

/**
 * Las reglas difusas
 * Contienen una lista de expresiones difusas como premisas - parte previa al THEN -
 * y una expresión difusa como conclusión.
 */
public class ReglaDifusa {
    protected ArrayList<ExpresionDifusa> premisas;
    protected ExpresionDifusa conclusion;

    //constructor 1
    public ReglaDifusa(ArrayList<ExpresionDifusa> _premisa, ExpresionDifusa _conclusion) {
        premisas = _premisa;
        conclusion = _conclusion;
    }
    //construcotr nuevo. A partir de secuencia de carcteres
    //descompone la regla escrita en premisas y en una conclusión y a continuaicón descompone cada parte en una expresión difusa.
    //split: para dividir una cadena en subcadenas
    //toUpper: pasar a mayúsculas
    //replaceFirst: elimina cierto número de caracteres.
    public ReglaDifusa(String reglastr, ControladorDifuso controlador){
        reglastr = reglastr.toUpperCase();

        String[] regla = reglastr.split(" THEN ");

        if (regla.length == 2) {
            regla[0] = regla[0].replaceFirst("IF ", "").trim();
            String[] premisasStr = regla[0].split(" AND ");
            premisas = new ArrayList<>();

            for (String exp : premisasStr) {
                String[] partes = exp.trim().split(" IS ");
                
                if (partes.length == 2) {
                    ExpresionDifusa expDifusa = new ExpresionDifusa(controlador.VariableLinguisticaPorNombre(partes[0]), partes[1]);
                    premisas.add(expDifusa);
                }
            }
            String[] concluStr = regla[1].trim().split(" IS ");

            if (concluStr.length == 2) {
                conclusion = new ExpresionDifusa(controlador.VariableLinguisticaPorNombre(concluStr[0]), concluStr[1]);
            }
        }
    }


    /**
     * Aplicar regla a un problema numérico concreto. Esto produce un conjunto difuso.
     * @param problema
     * @return
     */
    ConjuntoDifuso Aplicar(ArrayList<ValorNumerico> problema) {
        double grado = 1;

        for (ExpresionDifusa premisa : premisas) {
            double gradoLocal = 0;
            ValorLinguistico vl = null;

            for (ValorNumerico pb : problema) {
                if (premisa.varL.equals(pb.vl)) {
                    vl = premisa.varL.valorLinguisticoPorNombre(premisa.nombreValorLinguistico);

                    if (vl != null) {
                        gradoLocal = vl.ValorDePertenencia(pb.valor);
                        break;
                    }
                }
            }
            if (vl == null) {
                return null;
            }
            grado = Math.min(grado, gradoLocal);
        }
        return conclusion.varL.valorLinguisticoPorNombre(conclusion.nombreValorLinguistico).conjuntoDifuso.MultiplicarPor(grado);
    }

    
}
