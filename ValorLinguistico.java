package AI_2;

/**
 * Un nombre asociado a un conjunto difuso que lo caracteriza.
 * La clase tiene: 2 atributos
 * Posee 2 métodos: constructor y ...
 * método que devuelve el grado de pertenencia de un valor numérico determinado.
 * Este método invoca al método del conjunto difuso.
 * 
 * @author
 * @version
 */
public class ValorLinguistico {
    protected ConjuntoDifuso conjuntoDifuso;

    //nombre del valor
    protected String nombre;
    //constructor
    public ValorLinguistico(String _nombre,ConjuntoDifuso _cd){
        conjuntoDifuso = _cd;
        nombre = _nombre;
    }

    double ValorDePertenencia(double valor){
        return conjuntoDifuso.ValorDePertenencia(valor);
    }
}
