package AI_2;

public class ConjuntoDifusoTrapecio extends ConjuntoDifuso {
    //constructor
    public ConjuntoDifusoTrapecio(double min, double max, double inicioBase, double inicioMeseta, double finMeseta, double finBase){
        super(min, max);
        Agregar(new Punto2D(min, 0));
        Agregar(new Punto2D(inicioBase, 0));
        Agregar(new Punto2D(inicioMeseta, 1));
        Agregar(new Punto2D(finMeseta, 1));
        Agregar(new Punto2D(finBase, 0));
        Agregar(new Punto2D(max, 0));
    }
}

/**
 * Necesita 4 puntos
 * el extremo izquierdo de la base - inicioBase -
 * el extremo izquierdo de la meseta - inicioMeseta -
 * extremo derecho - FinMeseta -
 * extremo izquierdo - finBase -
 */