package AI_2;

/**
 * Es preciso asociar una variable lingüística con su valor numérico correspondiente.
 * 
 */
public class ValorNumerico {
    protected VariableLinguistica vl;
    protected double valor;

    //constructor
    public ValorNumerico(VariableLinguistica _vl, double _valor) {
        vl = _vl;
        valor = _valor;
    }
}
