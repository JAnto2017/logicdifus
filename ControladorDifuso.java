package AI_2;

import java.util.ArrayList;

/**
 * Permite gestionar las distintas variables lingüísticas recibidas como entrada.
 * La variable lingüística de salida.
 * Las distintas reglas.
 * 
 * 5 atributos.
 * 1 constructor.
 * 3 métodos.
 */
public class ControladorDifuso {
    protected String nombre;
    protected ArrayList<VariableLinguistica> entradas;
    protected VariableLinguistica salida;
    protected ArrayList<ReglaDifusa> reglas;
    protected ArrayList<ValorNumerico> problema;

    //constructor
    public ControladorDifuso(String _nombre) {
        nombre = _nombre;
        entradas = new ArrayList<>();
        reglas = new ArrayList<>();
        problema = new ArrayList<>();
    }

    //agregar una variable lingüística como entrada
    public void AgregarVariableEntrada(VariableLinguistica vl) {
        entradas.add(vl);
    }
    //agregar una variable lingüística como salida
    //una única posibilidad: reemplaza la existente si es necesario
    public void AgregarVariableSalida(VariableLinguistica vl) {
        salida = vl;
    }
    //agregar una regla
    public void AgregarRegla(ReglaDifusa regla) {
        reglas.add(regla);
    }
    //agregar un valor numérico como entrada
    public void AgregarValorNumerico(VariableLinguistica var, double valor) {
        problema.add(new ValorNumerico(var, valor));
    }
    //poner a cero el problema (para pasar al siguiente caso)
    public void BorrarValoresNumericos() {
        problema.clear();
    }
    //encontrar una variable lingüística a partir de su nombre
    public VariableLinguistica VariableLinguisticaPorNombre(String nombre) {
        for (VariableLinguistica var : entradas) {
            if (var.nombre.equalsIgnoreCase(nombre)) {
                return var;
            }
        }

        if (salida.nombre.equalsIgnoreCase(nombre)) {
            return salida;
        }
        return null;
    }

    /**
     * método principal.
     * Permite resolver un problema difuso y devolver el valor numérico esperado.
     * @return
     */
    public double Resolver() {
        //inicialización del conjunto difuso resultante
        ConjuntoDifuso resultado = new ConjuntoDifuso(salida.valorMin, salida.valorMax);
        resultado.Agregar(salida.valorMin,0);
        resultado.Agregar(salida.valorMax, 0);

        //aplicación de las reglas
        //y modificación del conjunto difuso resultante
        for (ReglaDifusa regla : reglas) {
            resultado = resultado.O(regla.Aplicar(problema));
        }

        //defuzzificación
        return resultado.Baricentro();
    }

    /**
     * Permite crear una regla no a partir de un objeto - ReglaDifusa -
     * sino a partir de una cadena de caracteres.
     * @param reglastr
     */
    public void AgregarRegla(String reglastr) {
        ReglaDifusa regla = new ReglaDifusa(reglastr, this);
        reglas.add(regla);
    }
}
