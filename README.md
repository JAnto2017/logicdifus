## Push an existing folder ##
#### cd existing_folder ####
+ git init --initial-branch=main
+ git remote add origin https://gitlab.com/JAnto2017/logicdifus.git
+ git add .
+ git commit -m "Initial commit"
+ git push -u origin main

## Push an existing Git repository ##
#### cd existing_repo ####
+ git remote rename origin old-origin
+ git remote add origin https://gitlab.com/JAnto2017/logicdifus.git
+ git push -u origin --all
+ git push -u origin --tags

## LÓGICA DIFUSA ##

Nooción de imprecisión NO es igual incertidumbre.

### INCERTIDUMBRE ###
Es lo contrario a certidumbre.

### IMPRECISIÓN Y SUBJETIVIDAD ###
Se manifiesta cuando falta, precisión.

### LÓGICA BOOLEANA Y/O DIFUSA ###

### FUNCIONES DE PERTENENCIA ###
La función de pertenencia, permite definir un conjunto difuso, que posee límites que no son claros, sino progresivos.

Un conjunto difuso, puede utilizar distintas funciones de pertenencia, que asocian todas un grado de pertenencia.

Existen 5 tipos:
1. Triangular: único valor y dos fases de transición lineales (antes y después del valor).
2. Trapezoidal: dispone de mesata, dos fases de transición lineales, antes y después de la meseta.
3. 1/2 Trapezoidal (izquierda o derecha): valores situados antes o después de un valor son verdaderos. Una fase de transición lineal entre valores nulos y meseta (valores verdaderos).
4. Función Gaussiana (campana).
5. Función sigmoide: parte de la función 1/2 trapecio, sin ángulos.

Cada función de pertenencia debe asociarse con un grado comprendido entre 0 y 1 para cada valor potencial del dominio.

## OPERADORES EN CONJUNTOS DIFUSOS ##
Existen tres operadores de composición elemental:
1. Unión O.
2. Intersección Y.
3. Negación NO.

Ejemplos: 
- Negación de A             => NO A     => ¬A
- La unión de A y B         => A O B    => A u B
- La intersección de A y B  => A Y B 

## REGLAS DIFUSAS ##
Las reglas utilizan valores difusas en lugar de valores numéricos.